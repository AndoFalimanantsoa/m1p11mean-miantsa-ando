const mongoose = require('mongoose');

// Définir le schéma de la citation
const managerSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },

    // Ajoutez d'autres champs si nécessaire
});


const Manager = mongoose.model('Manager', managerSchema); // Spécifiez le nom de la collection ici

module.exports = Manager;