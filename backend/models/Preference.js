const mongoose = require('mongoose');

// Définir le schéma de la citation
const preferenceSchema = new mongoose.Schema({
    note: {
        type: Number,
        required: true
    },
    clientId: {
        type: mongoose.Schema.Types.ObjectId,
        ref:'Client',
        required: true
    },
    
    serviceId: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Service' 
    },

    employeId: { 
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'Employe' 
    }

    // Ajoutez d'autres champs si nécessaire
});

const Preference = mongoose.model('Preference', preferenceSchema); // Spécifiez le nom de la collection ici

module.exports = Preference;