const mongoose = require('mongoose');

// Définir le schéma de la citation
const horaireEmployeSchema = new mongoose.Schema({
    employe: { type: mongoose.Schema.Types.ObjectId, ref: 'Employe' },
    matineeDebut: {
        type: Date
    },
    matineeFin: {
        type: Date
    },
    apresMidiDebut: {
        type: Date
    },
    apresMidiFin: {
        type: Date
    }
});

const HoraireEmploye = mongoose.model('HoraireEmploye', horaireEmployeSchema); // Spécifiez le nom de la collection ici

module.exports = HoraireEmploye;