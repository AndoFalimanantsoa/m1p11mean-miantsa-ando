const mongoose = require('mongoose');

// Définir le schéma de la citation
const employeSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    image: {
        type: String,
        required: true
    },
    password: {
        type: String
    },
    services: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Service' }] 

    // Ajoutez d'autres champs si nécessaire
});
employeSchema.pre('save', async function(next) {
    // Générez un mot de passe par défaut si aucun n'est fourni
    if (!this.password || this.password=='') {
      const defaultPassword = 'employe'; // Définissez votre mot de passe par défaut ici
      this.password = defaultPassword;
    }
  
    next();
});

const Employe = mongoose.model('Employe', employeSchema); // Spécifiez le nom de la collection ici

module.exports = Employe;