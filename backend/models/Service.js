const mongoose = require('mongoose');

// Définir le schéma de la citation
const serviceSchema = new mongoose.Schema({
    nomservice: {
        type: String,
        required: true
    },
    duree: {
        type: Number,
        required: true
    },
    prix: {
        type: Number,
        required: true
    },
    commission: {
        type: Number,
        required: true
    },
    image: {
        type: String, // Ajoutez le champ image de type String
        required: true // ou ajustez selon vos besoins
    }
});

// Créer le modèle de données 'Service' à partir du schéma
const Service = mongoose.model('Service', serviceSchema);

module.exports = Service;