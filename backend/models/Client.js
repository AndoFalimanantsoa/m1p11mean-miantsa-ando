const mongoose = require('mongoose');

// Définir le schéma de la citation
const clientSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },

    // Ajoutez d'autres champs si nécessaire
});


const Client = mongoose.model('Client', clientSchema); // Spécifiez le nom de la collection ici

module.exports = Client;