const mongoose = require('mongoose');

const offreSchema = new mongoose.Schema({
    nomoffre: {
        type: String,
        required: true
    },
    description: {
        type: String,
    },
    datedebut: {
        type: Date,
        required: true
    },
    datefin: {
        type: Date,
        required: true
    },
    services: [{ 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service' 
    }] ,
    tarif: {
        type: Number
    },
    remise: {
        type: Number
    }
    // Ajoutez d'autres champs si nécessaire
});

// Créer le modèle de données 'Quote' à partir du schéma
const Offre = mongoose.model('Offres', offreSchema); // Spécifiez le nom de la collection ici

module.exports = Offre;