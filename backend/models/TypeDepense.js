const mongoose = require('mongoose');

// Définir le schéma de la citation
const typedepenseSchema = new mongoose.Schema({
    nomtypedepense: {
        type: String,
        required: true
    }
});

// Créer le modèle de données 'TypeDepense' à partir du schéma
const Service = mongoose.model('TypeDepense', typedepenseSchema);

module.exports = Service;