const mongoose = require('mongoose');

// Définir le schéma de l'offre
const rendezvousSchema = new mongoose.Schema({
    idClient: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Client',
        required: true
    },
    dateRendezvous: {
        type: Date,
        required: true
    },
    idService: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Service',
        required: true
    },
    idEmploye: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Employe',
        required: true
    },
    etat: {
        type: Number,
        required: true
    },
});

// Créer le modèle de données 'Offre' à partir du schéma
const rendezvous = mongoose.model('Rendezvous', rendezvousSchema); // Spécifiez le nom de la collection ici

module.exports = rendezvous;