const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticateToken = require('../middleware/authenticateToken');
const { ObjectId, ClientEncryption } = require('mongodb');
const Client = require('../models/Client'); // Importez votre modèle de données de citation
const crypto = require('crypto');
const secretKey = crypto.randomBytes(32).toString('hex');

router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        const client = await Client.findOne({ email });
        if (!client) {
            return res.status(404).json({ message: "Nom d'utilisateur incorrect." });
        }

        if (client.password !== password) {
            return res.status(401).json({ message: "Mot de passe incorrect." });
        }
        const accessToken = jwt.sign({id: client.id , name: client.name, role: client.collection.name }, secretKey, { expiresIn: '1h' });
  

        res.status(200).json({ message: "Connexion réussie.", accesToken: accessToken, clientId: client.id });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la connexion." });
    }
});


// Route pour récupérer toutes les citations
router.get('/', async (req, res) => {
    try {
        const clients = await Client.find();
        res.json(clients);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour créer une nouvelle citation
router.post('/register', async (req, res) => {
    const { name, email, password, password1} = req.body;
    if (!name || !email || !password || !password1) {
        return res.status(400).json({ success: false, message: 'Please provide name, email, and password.' });
    }
    if (password !== password1) {
        return res.status(400).json({ success: false, message: 'Passwords do not match.' });
    }
    try {
        // Vérifiez si l'utilisateur existe déjà avec le même nom d'utilisateur ou la même adresse e-mail
        const existingClient = await Client.findOne({ $or: [{ name: name }, { email: email }] });
        if (existingClient) {
          return res.status(400).json({ success: false, message: 'name or email already in use.' });
        }
    
        // Si l'utilisateur n'existe pas déjà, créez un nouvel utilisateur
        const newClient = new Client({ name, email, password });
        await newClient.save();
        
        return res.status(201).json({ success: true, message: 'Client registered successfully.' });
      } catch (error) {
        console.error('Error registering client:', error);
        return res.status(500).json({ success: false, message: 'Error registering client.' });
      }
});

// Route pour mettre à jour une citation
router.put('/client/:id', async (req, res) => {
    const clientId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(clientId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid quote ID format' });
    }

    try {
        const updatedClient = await Client.findOneAndUpdate(
            { _id: new ObjectId(clientId) },
            { $set: { name: req.body.name, email: req.body.email, password: req.body.password } },
            { new: true }
        );
        res.json(updatedClient);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour supprimer une citation
router.delete('/client/:id', async (req, res) => {
    const clientId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(clientId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid client ID format' });
    }

    try {
        await Client.deleteOne({ _id: new ObjectId(clientId) });
        res.json({ message: 'Client deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/client/:_id', async (req, res) => {
    const idClient = req.params._id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(idClient);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid Employe ID format' });
    }
    try {
        const rdv = await Client.findOne({ _id: new ObjectId(idClient) });
        res.json(rdv);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;