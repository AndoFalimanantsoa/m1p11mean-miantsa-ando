const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
const rendezvous = require('../models/Rendezvous'); 
const clients = require('../models/Client');
const services = require('../models/Service');

router.get('/', async (req, res) => {
    try {
        const rdv = await rendezvous.find();
        res.json(rdv);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.post('/', async (req, res) => {
    const rdv = new rendezvous({
        idClient: req.body.idClient, 
        dateRendezvous: req.body.dateRendezvous, 
        idService: req.body.idService, 
        idEmploye: req.body.idEmploye,
        etat: 0, 
    });
    try {
       await rdv.save();
        res.status(201).json(rdv);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

router.put('/rdv/:id', async (req, res) => {
    const rdvId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(rdvId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid rdv ID format' });
    }

    try {
        const updatedRdv = await rendezvous.findOneAndUpdate(
            { _id: new ObjectId(rdvId) },
            { $set: { 
                dateRendezvous: req.body.dateRendezvous, 
                idService: req.body.idService, 
                idEmploye: req.body.idEmploye,
            } },  
            { new: true }
        );
        res.json(updatedRdv);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.put('/etat/:id', async (req, res) => {
    try {
        const rendezvousId = req.params.id;
        const newEtat = req.body.etat;
        if (newEtat !== 0 && newEtat !== 1) {
            return res.status(400).json({ message: 'L\'état doit être 0 ou 1' });
        }
        const rendezvousToUpdate = await rendezvous.findById(rendezvousId);

        if (!rendezvousToUpdate) {
            return res.status(404).json({ message: 'Rendez-vous introuvable' });
        }

        rendezvousToUpdate.etat = newEtat;

        const updatedRendezvous = await rendezvousToUpdate.save();

        res.json(updatedRendezvous);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/effectue/:idemploye', async (req, res) => {
    const etat = 1;
    const idemploye = req.params.idemploye;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(idemploye);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid Employe ID format' });
    }
    try {
        const rdvList = await rendezvous.find({ 
            idEmploye: new ObjectId(idemploye),
            etat: etat
        });
        const clientDetailsList = [];

        for (const rdv of rdvList) {
            const client = await clients.findOne({ "_id": rdv.idClient });
            const service = await services.findOne({ "_id": rdv.idService });
            clientDetailsList.push({
                rendezvous: rdv,
                client: client,
                service: service,
            });
        }
        res.json(clientDetailsList);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/afaire/:idemploye', async (req, res) => {
    const etat = 0;
    const idemploye = req.params.idemploye;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(idemploye);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid Employe ID format' });
    }
    try {
        const rdvList = await rendezvous.find({ 
            idEmploye: new ObjectId(idemploye),
            etat: etat
        });
        const clientDetailsList = [];

        for (const rdv of rdvList) {
            const client = await clients.findOne({ "_id": rdv.idClient });
            const service = await services.findOne({ "_id": rdv.idService });
            clientDetailsList.push({
                rendezvous: rdv,
                client: client,
                service: service,
            });
        }
        res.json(clientDetailsList);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/rdv/:_id', async (req, res) => {
    const rdvId = req.params._id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(rdvId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid Employe ID format' });
    }
    try {
        const rdv = await rendezvous.findOne({ _id: new ObjectId(rdvId) });
        res.json(rdv);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/rdv/client/:idclient', async (req, res) => {
    const idClient = req.params.idclient;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(idClient);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid Employe ID format' });
    }
    try {
        const rdv = await rendezvous.find({ idClient: new ObjectId(idClient) });
        res.json(rdv);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});


module.exports = router;