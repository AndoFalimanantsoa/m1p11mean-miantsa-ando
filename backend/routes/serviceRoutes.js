// serviceRoutes.js
const express = require('express');
const router = express.Router();
const multer = require('multer');
const fs = require('fs').promises;
const { ObjectId } = require('mongodb');
const Service = require('../models/Service'); // Importer votre modèle de données de service
const storage = multer.memoryStorage(); // Stocker les fichiers en mémoire (vous pouvez ajuster selon vos besoins)
const upload = multer({ storage: storage });

// Route pour récupérer tous les services
router.get('/', async (req, res) => {
    try {
        const services = await Service.find();
        res.json(services);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

router.get('/:id', async (req, res) => {
    try {
      const serviceId= req.params.id;
      const service = await Service.findOne(
        { _id: new ObjectId(serviceId) },
       );
        res.json(service);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
  });

// Route pour créer un nouveau service
router.post('/', upload.single('image'), async (req, res) => {
    try {
        if (!req.file) {
            return res.status(400).json({ message: 'Aucun fichier trouvé dans la requête' });
        }
        // Accédez au fichier téléchargé via req.file.buffer
        const imageBuffer = req.file.buffer;

        // Convertissez le tampon d'image en chaîne Base64
        const base64Image = imageBuffer.toString('base64');

        const service = new Service({
            nomservice: req.body.nomservice,
            duree: req.body.duree,
            prix: req.body.prix,
            commission: req.body.commission,
            image: base64Image
        });

        // Sauvegarder le service dans la base de données
        const newService = await service.save();

        res.status(201).json(newService);
    } catch (error) {
        console.error('Erreur lors du traitement du fichier:', error);
        res.status(500).json({ message: 'Erreur lors de l\'ajout du service' });
    }
});

// Route pour mettre à jour un service
router.put('/service/:id', async (req, res) => {
    const serviceId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(serviceId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid service ID format' });
    }

    try {
        const updatedService = await Service.findOneAndUpdate(
            { _id: new ObjectId(serviceId) },
            { $set: { nomservice: req.body.nomservice, duree: req.body.duree, prix: req.body.prix, commission: req.body.commission } }, // Correction des champs 
            { new: true }
        );
        res.json(updatedService);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour supprimer un service
router.delete('/service/:id', async (req, res) => {
    const serviceId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(serviceId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid service ID format' });
    }

    try {
        await Service.deleteOne({ _id: new ObjectId(serviceId) });
        res.json({ message: 'Service deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
