const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const { ObjectId } = require('mongodb');
const HoraireEmploye = require('../models/HoraireEmploye');
// Importez votre modèle de données de citation


router.get('/:id', async (req, res) => {
    try {
      const employeId= req.params.id;
      const employe = await HoraireEmploye.findOne(
        { employe: new ObjectId(employeId) },
       );
        res.json(employe);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
  });

router.post('/', async (req, res) => {
    let {employe, matineeDebut, matineeFin, apresMidiDebut, apresMidiFin } = req.body;
    console.log(employe);
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employe);
    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid service ID format' });
    }

    if(!matineeDebut){
        matineeDebut = new Date(0, 0, 0, 8, 0);
    }
    if(!matineeFin){
        matineeFin= new Date(0, 0, 0, 12, 0);
    }
    if(!apresMidiDebut){
        apresMidiDebut= new Date(0, 0, 0, 13, 0);
    }
    if(!apresMidiFin){
        apresMidiFin= new Date(0, 0, 0, 17, 0);
    }

    try {
        
        const newHoraire = new HoraireEmploye({employe, matineeDebut, matineeFin, apresMidiDebut, apresMidiFin});
        await newHoraire.save()


        res.status(202).json({ message: "Horaire enregistrée avec succès" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Une erreur est survenue" });
    }
});

router.put('/horaireEmploye/:id', async (req, res) => {
    const employeId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);
  
    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid service ID format' });
    }
  
    try {
        const updatedHoraire = await HoraireEmploye.findOneAndUpdate(
            { employe: new ObjectId(employeId) },
            { $set: 
                { 
                    matineeDebut: req.body.matineeDebut, 
                    matineeFin: req.body.matineeFin,
                    apresMidiDebut: req.body.apresMidiDebut,
                    apresMidiFin: req.body.apresMidiFin
                } 
            }, // Correction des champs 
            { new: true }
        );
        res.json(updatedHoraire);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
  });
  

module.exports = router;