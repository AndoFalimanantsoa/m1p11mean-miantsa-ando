// typedepenseRoutes.js
const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
const TypeDepense = require('../models/TypeDepense'); // Importer votre modèle de données de typedepense

// Route pour récupérer tous les types de dépense
router.get('/', async (req, res) => {
    try {
        const typedepenses = await TypeDepense.find();
        res.json(typedepenses);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour créer un nouveau type de dépense
router.post('/', async (req, res) => {
    const typedepense = new TypeDepense({
        nomtypedepense: req.body.nomtypedepense, // Correction du champ 'nomtypedepense'
    });
    try {
        const newTypeDepense = await typedepense.save();
        res.status(201).json(newTypeDepense);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Route pour mettre à jour un type de dépense
router.put('/typedepense/:id', async (req, res) => {
    const typedepenseId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(typedepenseId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid typedepense ID format' });
    }

    try {
        const updatedTypeDepense = await TypeDepense.findOneAndUpdate(
            { _id: new ObjectId(typedepenseId) },
            { $set: { nomtypedepense: req.body.nomtypedepense } }, // Correction des champs 
            { new: true }
        );
        res.json(updatedTypeDepense);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour supprimer un type de dépense
router.delete('/typedepense/:id', async (req, res) => {
    const typedepenseId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(typedepenseId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid type depense ID format' });
    }

    try {
        await TypeDepense.deleteOne({ _id: new ObjectId(typedepenseId) });
        res.json({ message: 'TypeDepense deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
