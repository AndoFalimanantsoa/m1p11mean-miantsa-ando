const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const { ObjectId } = require('mongodb');
const Manager = require('../models/Manager');
// Importez votre modèle de données de citation

const crypto = require('crypto');
const secretKey = crypto.randomBytes(32).toString('hex');

router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    try {
        const manager = await Manager.findOne({ email });
        if (!manager) {
            return res.status(404).json({ message: "Adresse email incorrect." });
        }

        if (manager.password !== password) {
            return res.status(401).json({ message: "Mot de passe incorrect." });
        }
        const accessToken = jwt.sign({id: manager.id , name: manager.name, role: manager.collection.name }, secretKey, { expiresIn: '1h' });
  

        res.status(200).json({ message: "Connexion réussie.", accesToken: accessToken, managerId: manager.id });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la connexion." });
    }
});

router.post('/', async (req, res) => {
    const { name, email, password} = req.body;
    if (!name || !email || !password ) {
        return res.status(400).json({ success: false, message: 'Veuillez remplir tous les champs.' });
    }
    try {
        // Vérifiez si l'utilisateur existe déjà avec le même nom d'utilisateur ou la même adresse e-mail
        const existingManager = await Manager.findOne({ $or: [{ name: name }, { email: email }] });
        if (existingManager) {
          return res.status(400).json({ success: false, message: 'Nom ou email déja utilisé.' });
        }
    
        // Si l'utilisateur n'existe pas déjà, créez un nouvel utilisateur
        const newManager = new Manager({ name, email, password });
        await newManager.save();
        
        return res.status(201).json({ success: true, message: 'Manager enregistré avec succès' });
      } catch (error) {
        console.error('Error registering client:', error);
        return res.status(500).json({ success: false, message: 'Erreur lors de l\'enregistrement' });
      }
});

router.get('/:id', async (req, res) => {
    try {
      const managerId= req.params.id;
      const manager = await Manager.findOne(
        { _id : new ObjectId(managerId) },
       );
        res.json(manager);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
  });


  

module.exports = router;