const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticateToken = require('../middleware/authenticateToken');
const { ObjectId, ClientEncryption } = require('mongodb');
const Employe = require('../models/Employe'); // Importez votre modèle de données de citation
const crypto = require('crypto');
const secretKey = crypto.randomBytes(32).toString('hex');
const multer = require('multer');
const path = require('path');


const uploadDirectory = "C:/Users/VIOTECH/Documents/GitHub/m1p11mean-miantsa-ando/frontend/src/assets/img";
const storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, uploadDirectory);
  },
  filename: function (req, file, cb) {
    const fileName = path.basename(file.originalname);
    cb(null,fileName);
    console.log('blibli ');
  }
});
const upload = multer({ storage: storage });

router.post('/', upload.single('image'), async (req, res) => {
  //const imagePath = req.file.path;
  const fileName = path.basename(req.file.originalname);
  const name = req.body.name;
  const password = req.body.password;
  const services=req.body.services;
  
  try {
    console.log('misitra eto lery ');
    const existingEmploye = await Employe.findOne({ $or: [{ name: name }] });
    if (existingEmploye) {
      
      return res.status(400).json({ success: false, message: 'Ce nom est déja utilisé' });
    }


    const newEmploye = new Employe({ name, image: fileName, password, services});
    console.log('eto tsisy tsony');
    console.log(fileName);
    console.log(newEmploye);
    await newEmploye.save();
    
    return res.status(201).json({ success: true, message: 'Employé enregistré avec succès' , employeId: newEmploye._id});
  } catch (error) {
    console.error('Une erreur est survenue:', error);
    return res.status(500).json({ success: false, message: 'une erreur est survenue lty eh'});
  }
});

router.put('/employe/updateImage/:id', upload.single('image'), async (req, res) => {
  //const imagePath = req.file.path;
  console.log(req.file);
  const fileName = path.basename(req.file.originalname);
  const name = req.body.name;
  const employeId = req.params.id;
  const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);

  if (!isValidObjectId) {
      return res.status(400).json({ error: 'Invalid service ID format' });
  }
  
  try {
    const updatedEmploye = await Employe.findOneAndUpdate(
        { _id: new ObjectId(employeId) },
        { $set: { name: name, image: fileName} }, // Correction des champs 
        { new: true }
    );

    return res.status(201).json({ success: true, message: 'Employé modifié avec succès' });
} catch (err) {
    res.status(500).json({ message: err.message });
}
});


router.put('/employe/updateName/:id', async (req, res) => {

  const name = req.body.name;
  const employeId = req.params.id;
  const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);

  if (!isValidObjectId) {
      return res.status(400).json({ error: 'Invalid service ID format' });
  }
  
  try {
    const updatedEmploye = await Employe.findOneAndUpdate(
        { _id: new ObjectId(employeId) },
        { $set: { name: name} }, // Correction des champs 
        { new: true }
    );

    return res.status(201).json({ success: true, message: 'Employé modifié avec succès' });
} catch (err) {
    res.status(500).json({ message: err.message });
}
});




router.post('/login', async (req, res) => {
    const { name, password } = req.body;

    try {
        const employe = await Employe.findOne({ name });
        if (!employe) {
            return res.status(404).json({ message: "Nom d'utilisateur incorrect." });
        }

        if (employe.password !== password) {
            return res.status(401).json({ message: "Mot de passe incorrect." });
        }
        const accessToken = jwt.sign({id: employe.id , name: employe.name, role: employe.collection.name }, secretKey, { expiresIn: '1h' });
  

        res.status(200).json({ message: "Connexion réussie.", accesToken: accessToken, employeId: employe.id });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Erreur lors de la connexion." });
    }
});



router.get('/', async (req, res) => {
  try {
      const employes = await Employe.find().populate('services');
      res.json(employes);
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const employeId= req.params.id;
    const employe = await Employe.findOne(
      { _id: new ObjectId(employeId) },
     );
      res.json(employe);
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});

router.put('/employe/:id', async (req, res) => {
  const employeId = req.params.id;
  const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);

  if (!isValidObjectId) {
      return res.status(400).json({ error: 'Invalid service ID format' });
  }

  try {
      const updatedEmploye = await Employe.findOneAndUpdate(
          { _id: new ObjectId(employeId) },
          { $set: { name: req.body.name, services: req.body.services} }, // Correction des champs 
          { new: true }
      );
      res.json(updatedEmploye);
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});

router.delete('/employe/:id', async (req, res) => {
  const employeId = req.params.id;
  const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);

  if (!isValidObjectId) {
      return res.status(400).json({ error: 'Invalid service ID format' });
  }

  try {
      await Employe.deleteOne({ _id: new ObjectId(employeId) });
      res.json({ message: 'Employe deleted successfully' });
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});


router.get('/byService/:id',  async (req, res) => {
    const serviceId = req.params.id;
    
    try {
      
      const employes = await Employe.find({ services: serviceId });
      res.json(employes);
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
});

router.put('/employe/updatePassword/:id', async (req, res) => {
  const employeId = req.params.id;
  const { password, password1, password2 } = req.body;
  const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);

  if (!isValidObjectId) {
      return res.status(400).json({ error: 'Invalid service ID format' });
  }

  const employe= await Employe.findOne({_id: new ObjectId(employeId)});
 
  if(employe.password !== password){
    return res.status(401).json({ message: "Mot de passe actuel incorrect." });
  }

  if(password1 !== password2){
    return res.status(401).json({ message: "Veuillez re-taper le mot de passe." });
  }

  try {
      const updatedEmploye = await Employe.findOneAndUpdate(
          { _id: new ObjectId(employeId) },
          { $set: { password: password1} }, // Correction des champs 
          { new: true }
      );

      return res.status(201).json({ success: true, message: 'Mot de passe modifié avec succès' });
  } catch (err) {
      res.status(500).json({ message: err.message });
  }
});


module.exports = router;