const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const authenticateToken = require('../middleware/authenticateToken');
const { ObjectId, ClientEncryption } = require('mongodb');
const Preference = require('../models/Preference'); // Importez votre modèle de données de citation
const crypto = require('crypto');
const secretKey = crypto.randomBytes(32).toString('hex');

router.post('/', async (req, res) => {
    const {note, clientId, serviceId, employeId } = req.body;

    try {
        const existingPref = await Preference.findOne({clientId: clientId, serviceId: serviceId, employeId: employeId});
        if (existingPref) {
            await existingPref.updateOne({note: note});
            return res.status(202).json({ message: 'Préférence modifiée avec succès' });
        }
        const newPref = new Preference({note,clientId,serviceId,employeId});
        await newPref.save()


        res.status(202).json({ message: "Préférence enregistrée avec succès" });
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: "Une erreur est survenue" });
    }
});

router.put('/preference/:id', async (req, res) => {
    const employeId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(employeId);
  
    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid service ID format' });
    }
  
    try {
        const updatedEmploye = await Employe.findOneAndUpdate(
            { _id: new ObjectId(employeId) },
            { $set: { name: req.body.name, services: req.body.services} }, // Correction des champs 
            { new: true }
        );
        res.json(updatedEmploye);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
  });
  

module.exports = router;