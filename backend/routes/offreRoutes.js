// Offres.js
const express = require('express');
const router = express.Router();
const { ObjectId } = require('mongodb');
const Offres = require('../models/Offres');
const Services = require('../models/Service');

router.get('/', async (req, res) => {
    try {
        const offres = await Offres.find();
        const offresDetails = [];

        for (const offre of offres) {
            const servicesDetails = [];
            
            for (const service of offre.services) {
                const serv = await Services.findOne({ "_id": service._id });
                servicesDetails.push(serv);
            }

            offresDetails.push({
                offre: offre,
                services: servicesDetails,
            });
        }
        res.json(offresDetails);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour créer une nouvelle offre
router.post('/', async (req, res) => {
    const offre = new Offres({
        nomoffre: req.body.nomoffre, 
        description: req.body.description, 
        datefin: req.body.datefin,
        datedebut: req.body.datedebut,
        remise: req.body.remise, 
        prix: req.body.prix,
        services: req.body.services
    });
    try {
        const newOffre = await offre.save();
        res.status(201).json(newOffre);
    } catch (err) {
        res.status(400).json({ message: err.message });
    }
});

// Route pour mettre à jour une offre
router.put('/offre/:id', async (req, res) => {
    const offreId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(offreId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid offre ID format' });
    }

    try {
        const updatedOffre = await Offres.findOneAndUpdate(
            { _id: new ObjectId(offreId) },
            { $set: { 
                idservice: req.body.idservice,
                datedebut: req.body.datedebut,
                datefin: req.body.datefin, 
                remise: req.body.remise 
            } }, // Correction des champs 
            { new: true }
        );
        res.json(updatedOffre);
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

// Route pour supprimer une offre
router.delete('/offre/:id', async (req, res) => {
    const offreId = req.params.id;
    const isValidObjectId = /^[0-9a-fA-F]{24}$/.test(offreId);

    if (!isValidObjectId) {
        return res.status(400).json({ error: 'Invalid offre ID format' });
    }

    try {
        await Offres.deleteOne({ _id: new ObjectId(offreId) });
        res.json({ message: 'Offre deleted successfully' });
    } catch (err) {
        res.status(500).json({ message: err.message });
    }
});

module.exports = router;
