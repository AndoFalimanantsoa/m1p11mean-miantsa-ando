const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');
const cors = require('cors');
const mongoose = require('mongoose');
const fs = require('fs');
const clientsRoutes = require('./routes/clientsRoutes');
const serviceRoutes = require('./routes/serviceRoutes');
const typedepenseRoutes = require('./routes/typedepenseRoutes');
const offreRoutes = require('./routes/offreRoutes');
const rendezvousRoutes = require('./routes/rendezvousRoutes');
const employesRoutes = require('./routes/employesRoutes');
const preferencesRoutes = require('./routes/preferencesRoutes');
const horairesEmployesRoutes = require('./routes/horairesEmployesRoutes');
const managersRoutes = require('./routes/managersRoutes');


const app = express();

// Middleware
app.use(cors());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Connexion à MongoDB
const connectionString = 'mongodb+srv://ando:Tv8h2LLA6nOgfZhq@cluster0.pj1fhjt.mongodb.net/MEAN?retryWrites=true&w=majority';
mongoose.connect(connectionString)
    .then(() => console.log('MongoDB Connected'))
    .catch(err => console.error(err));

// Définir le chemin vers le répertoire 'dist' de votre application Angular
const angularDistPath = path.join(__dirname, 'frontend/dist/frontend/browser');

// Servir les fichiers statiques depuis le répertoire 'dist'
app.use(express.static(angularDistPath));

// Utiliser les routes des citations et des utilisateurs
app.use('/api/clients', clientsRoutes);
app.use('/api/services', serviceRoutes);
app.use('/api/typedepenses', typedepenseRoutes);
app.use('/api/offres', offreRoutes);
app.use('/api/rendezvous', rendezvousRoutes);
app.use('/api/employes', employesRoutes);
app.use('/api/preferences', preferencesRoutes);
app.use('/api/horairesEmployes', horairesEmployesRoutes);
app.use('/api/managers', managersRoutes);

// Gérer le routage côté client pour les routes d'Angular
app.get('*', (req, res) => {
    res.sendFile(path.join(angularDistPath, 'index.html'));
});

// Démarrer le serveur
const PORT = process.env.PORT || 3000;
app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}`);
});