import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule, NgFor } from '@angular/common';
import { EmployeApiServiceService } from '../../Service/service-employe/employe-api-service.service.service';

@Component({
  selector: 'app-profil',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './profil.component.html',
  styleUrl: './profil.component.css'
})
export class ProfilComponent implements OnInit{
  employe: any = {};
  services: any[] = [];
  password: string = '';
  password1: string = '';
  password2: string = '';
  errPassword: string = '';
  name: string = '';
  image: File | null = null;


  constructor(private myApiService: EmployeApiServiceService) {}
  ngOnInit() {
    this.loadEmploye();
  }

  loadEmploye(){
    const employeId = localStorage.getItem('employeId');
    if(employeId){
      this.myApiService.getOneEmploye(employeId).subscribe(
        (data: any) => {
          console.log('Données récupérées avec succès :', data);
          this.employe = data;
          this.assignService(this.employe.services);
        },
        (error) => {
          console.error('Erreur côté client :', error);
          console.error('Réponse serveur :', error.error);
          // Affichez l'erreur pour en savoir plus
        }
      );
    }
  }

  assignService(services: string[]){
    for (let i = 0; i < services.length; i++) {
      this.myApiService.getOneService(services[i]).subscribe(
        (data: any) => {
          this.services.push(data);
        },
        (error) => {
          console.error('Erreur côté client :', error);
          console.error('Réponse serveur :', error.error);
          // Affichez l'erreur pour en savoir plus
        }
      );
    }
  }
  //////////////////////////////// Drag and Drop //////////////////////////////
  onFileSelected(event: any): void {
    if (event.target.files[0]) {
      this.image = event.target.files[0];
      if(this.image){
        console.log("image: "+this.image.name);
      }
     
    }
  }

  getImageUrl(image: File | null): string {
    if (image) {
      return URL.createObjectURL(image);
    } else {
      // Retournez une URL par défaut si aucune image n'est sélectionnée
      return '';
    }
  }

  onDragOver(event: DragEvent): void {
    event.preventDefault();
  }

  onDragEnter(event: DragEvent): void {
    event.preventDefault();

    const dropzone = event.target as HTMLElement;
    dropzone.classList.add('dragover');
    
  }
  onDragLeave(event: DragEvent): void {
    event.preventDefault();
    // Ajoutez ici des styles CSS pour réinitialiser les styles de survol
    const dropzone = event.target as HTMLElement;
    dropzone.classList.remove('dragover');
  }
  onDrop(event: DragEvent): void {
    event.preventDefault();
    const dropzone = event.target as HTMLElement;
    dropzone.classList.remove('dragover');
    const files = event.dataTransfer?.files;
    if (files && files.length > 0) {
      this.image = files[0];
      console.log("image: "+this.image.name);
    }
  }
  updateEmploye() {
    if (!this.image && this.name){
      this.myApiService.updateNameEmploye(this.employe._id,this.name).subscribe(
        (response) => {
        console.log('Employé modifié avec succès:', response);
        this.loadEmploye();
        },
        (error) => {
        console.error('Erreur lors de la modification de l\'employé:', error);
        }
      );
    }
    if (this.image){
      let name='';
      console.log("this.name:" + this.name);
      if(this.name !== ''){
        name=this.name;
      }
      else{
        name=this.employe.name;
      }
      this.myApiService.updateEmploye(this.employe._id,name, this.image).subscribe(
      (response) => {
        console.log('Employé modifié avec succès:', response);
        this.loadEmploye();
      },
      (error) => {
        console.error('Erreur lors de la modification de l\'employé:', error);
      }
    );

    }
    
  }

  ngAfterViewInit(): void {
    // Mettre à jour l'image après que la vue ait été initialisée
    this.updateImageSrc();
  }

  updateImageSrc(): void {
    const imgElement = document.getElementById('profile-image') as HTMLImageElement;
    if (imgElement && this.image) {
      imgElement.src = this.getImageUrl(this.image);
    }
  }

  updatePassword(employeId: string): void {
    const password= this.password;
    const password1= this.password1;
    const password2= this.password2;
  
    this.myApiService.updatePassword(employeId, password, password1, password2).subscribe(
      (response) => {
        console.log('Employé mise à jour avec succès:', response);
        this.loadEmploye();
      },
      (error) => {
        console.error('Erreur lors de la mise à jour de l\'employé:', error);
        this.errPassword=error.error.message;
        // Gérez les erreurs en conséquence
      }
    );
  }

}
