import { Component, OnInit } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { EmployeApiServiceService } from '../../Service/service-employe/employe-api-service.service.service';

@Component({
  selector: 'app-horaire',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './horaire.component.html',
  styleUrl: './horaire.component.css'
})
export class HoraireComponent implements OnInit {
  horaire: any = {};
  matineeDebut: string = ''; 
  matineeFin: string = ''; 
  apresMidiDebut: string = ''; 
  apresMidiFin: string = ''; 
  employeId = localStorage.getItem('employeId');
  err: string = '';

  constructor(private myApiService: EmployeApiServiceService) {}

  ngOnInit(): void {
    this.loadHoraire();
  }

  loadHoraire(): void {
    if (this.employeId) {
      this.myApiService.getHoraire(this.employeId).subscribe(
        (data: any) => {
          this.horaire = data;
        },
        (error) => {
          console.error('Erreur côté client :', error);
          console.error('Réponse serveur :', error.error);
        }
      );
    }
  }

  updateHoraire(): void {
    // Vérifiez si les champs d'heure sont vides
    if (!this.matineeDebut || !this.matineeFin || !this.apresMidiDebut || !this.apresMidiFin) {
      console.error('Veuillez entrer des heures valides pour toutes les périodes de travail.');
      this.err = 'Heure non valide';
      return;
    }

    // Récupérez la durée des périodes de travail
    const matineeDebutTime = parseInt(this.matineeDebut.split(":")[0],10) * 60 * 60 * 1000 + parseInt(this.matineeDebut.split(":")[1],10) * 60 * 1000;
    const matineeFinTime = parseInt(this.matineeFin.split(":")[0],10) * 60 * 60 * 1000 + parseInt(this.matineeFin.split(":")[1],10) * 60 * 1000;
    const apresMidiDebutTime = parseInt(this.apresMidiDebut.split(":")[0],10) * 60 * 60 * 1000 + parseInt(this.apresMidiDebut.split(":")[1],10) * 60 * 1000;
    const apresMidiFinTime = parseInt(this.apresMidiFin.split(":")[0],10) * 60 * 60 * 1000 + parseInt(this.apresMidiFin.split(":")[1],10) * 60 * 1000;

    console.log(this.matineeDebut);
    const matineeDuration = matineeFinTime - matineeDebutTime;
    const apresMidiDuration = apresMidiFinTime - apresMidiDebutTime;
    

    // Convertir la durée en heures
    const matineeHours = matineeDuration / (1000 * 60 * 60);
    const apresMidiHours = apresMidiDuration / (1000 * 60 * 60);

    // Vérifiez si les heures de travail dépassent les limites
    if (parseInt(this.matineeDebut.split(":")[0],10) < 7 || parseInt(this.apresMidiFin.split(":")[0],10) > 18) {
      console.error('Vous ne pouvez modifier les horaires en dehors des heures de travail');
      this.err = 'Vous ne pouvez modifier les horaires en dehors des heures de travail';
      return;
    }

    // Vérifiez si la durée totale de travail est inférieure à 8 heures
    if (matineeHours + apresMidiHours < 8) {
      console.error('Vous devez remplir les 8h de travail');
      this.err = 'Vous devez remplir les 8h de travail';
      return;
    }

    // Construire l'objet à envoyer pour la mise à jour
    const updatedHoraire = {
      matineeDebut: '1970-01-01T'+this.matineeDebut+':00',
      matineeFin: '1970-01-01T'+this.matineeFin+':00',
      apresMidiDebut: '1970-01-01T'+this.apresMidiDebut+':00',
      apresMidiFin: '1970-01-01T'+this.apresMidiFin+':00'
    };
    console.log(updatedHoraire);

    // Effectuez la mise à jour de l'horaire
    if (this.employeId) {
      this.myApiService.updateHoraire(this.employeId, updatedHoraire).subscribe(
        (response) => {
          console.log('Horaire mis à jour avec succès :', response);
          this.loadHoraire();
        },
        (error) => {
          console.error('Erreur lors de la mise à jour de l\'horaire :', error);
        }
      );
    }
  }
}
