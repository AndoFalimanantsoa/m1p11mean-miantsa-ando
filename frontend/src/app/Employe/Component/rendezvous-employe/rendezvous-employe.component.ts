import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { RendezvousService } from '../../Service/rendezvous/rendezvous.service';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClientApiServiceService } from '../../../Client/Service/service-client/client-api-service.service';

@Component({
  selector: 'app-rendezvous-employe',
  standalone: true,
  imports: [FormsModule, CommonModule],
  templateUrl: './rendezvous-employe.component.html',
  styleUrl: './rendezvous-employe.component.css'
})
export class RendezvousEmployeComponent implements OnInit {
  inacheves: any[] = [];
  effectues: any[] = [];

  constructor(private rdvService: RendezvousService,private clientService: ClientApiServiceService) { }

  ngOnInit(): void {
    this.loadRdvAfaire();
    this.loadRdvEffectue();
  }
  loadRdvAfaire(){
    const employeId: string = localStorage.getItem("employeId") + '';
    this.rdvService.getRdvAfaire(employeId).subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.inacheves = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
      }
    );
  }

  loadRdvEffectue(){
    const employeId: string = localStorage.getItem("employeId") + '';
    this.rdvService.getRdvEffectue(employeId).subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.effectues = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
      }
    );
  }

  onCheckboxChange(rdv: any): void {
    let newEtat = 1;
    console.log("idClient: "+ rdv.rendezvous.idClient);
    if(rdv.rendezvous.etat == 1){
      newEtat = 0;
    }
    this.rdvService.updateEtat(rdv.rendezvous._id, newEtat).subscribe(
      (response) => {
        console.log('Rdv mis à jour', response);
         this.loadRdvAfaire();
          this.loadRdvEffectue();
      },
      (error) => {
        console.error('Erreur lors de la mise à jour du rdv:', error);
      }
    );
  }
  
}
