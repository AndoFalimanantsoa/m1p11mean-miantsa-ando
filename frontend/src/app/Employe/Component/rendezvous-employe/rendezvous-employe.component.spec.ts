import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RendezvousEmployeComponent } from './rendezvous-employe.component';

describe('RendezvousEmployeComponent', () => {
  let component: RendezvousEmployeComponent;
  let fixture: ComponentFixture<RendezvousEmployeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [RendezvousEmployeComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(RendezvousEmployeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
