import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-accueil-employe',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  templateUrl: './accueil-employe.component.html',
  styleUrl: './accueil-employe.component.css'
})
export class AccueilEmployeComponent {

}
