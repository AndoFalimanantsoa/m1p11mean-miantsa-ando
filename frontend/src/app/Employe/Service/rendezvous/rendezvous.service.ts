import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RendezvousService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/rendezvous';

  constructor(private http: HttpClient) { }

  addRendezvous(rendezvousData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/';
    return this.http.post<any>(apiUrl, rendezvousData);
  }

  updateRendezvous(rendezvousId: string, rendezvousData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/rdv/' + rendezvousId;
    return this.http.put<any>(apiUrl, rendezvousData);
  }

  getRdvEffectue(idemploye: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/effectue/' + idemploye;
    return this.http.get<any[]>(apiUrl);
  }

  getRdvAfaire(idemploye: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/afaire/' + idemploye;
    return this.http.get<any[]>(apiUrl);
  }

  updateEtat(id: string, etat: Number): Observable<any[]> {
    const apiUrl = this.baseUrl + '/etat/' + id;
    const updatedRdv = { etat: etat };
    return this.http.put<any>(apiUrl, updatedRdv);
  }
}
