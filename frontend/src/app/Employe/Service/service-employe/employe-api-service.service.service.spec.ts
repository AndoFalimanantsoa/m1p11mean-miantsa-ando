import { TestBed } from '@angular/core/testing';

import { EmployeApiServiceServiceService } from './employe-api-service.service.service';

describe('EmployeApiServiceServiceService', () => {
  let service: EmployeApiServiceServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(EmployeApiServiceServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
