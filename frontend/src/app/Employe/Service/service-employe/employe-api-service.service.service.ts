import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class EmployeApiServiceService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/employes';

  constructor(private http: HttpClient) {}

  login(name: string, password: string): Observable<any> {
    const apiUrl = this.baseUrl + '/login';
    return this.http.post<any>(apiUrl, {name, password});
  }

  getEmployes(): Observable<any[]> {
    const apiUrl = this.baseUrl + '/';
    return this.http.get<any[]>(apiUrl);
  }

  getOneEmploye(employeId: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/'+employeId;
    return this.http.get<any[]>(apiUrl);
  }

  updatePassword(employeId: string, password: string, password1: string, password2: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/employe/updatePassword/' + employeId;
    return this.http.put<any[]>(apiUrl, {password: password, password1:password1, password2:password2});
  }

  updateEmploye(employeId: string, name: string, image: File): Observable<any> {
    const apiUrl = this.baseUrl + '/employe/updateImage/'+ employeId;
    const formData = new FormData();

    formData.append('name', name);
    formData.append('image', image);
    
    return this.http.put<any>(apiUrl, formData);
  }

  updateNameEmploye(employeId: string, name: string): Observable<any> {
    const apiUrl = this.baseUrl + '/employe/updateName/'+ employeId;
    return this.http.put<any>(apiUrl, {name: name});
  }

  /////////////////////    Services    ////////////////////////////////
  private baseUrlServ = 'http://localhost:3000/api/services';
  getOneService(serviceId: string): Observable<any[]> {
    const apiUrl = this.baseUrlServ + '/'+serviceId;
    return this.http.get<any[]>(apiUrl);
  }


  ////////////////////    Horaire     //////////////////////////////////
  private baseUrlHoraire = 'http://localhost:3000/api/horairesEmployes';
  getHoraire(employeId: string): Observable<any[]> {
    const apiUrl = this.baseUrlHoraire + '/'+employeId;
    return this.http.get<any[]>(apiUrl);
  }

  addHoraire(employe: string, matineeDebut: Date, matineeFin: Date, apresMidiDebut: Date, apresMidiFin: Date): Observable<any[]> {
    const apiUrl = this.baseUrlHoraire + '/';
    return this.http.post<any[]>(apiUrl, {employe:employe, matineeDebut: matineeDebut, matineeFin: matineeFin, apresMidiDebut: apresMidiDebut, apresMidiFin: apresMidiFin});
  }

  updateHoraire(employe: string, updatedHoraire: any): Observable<any[]> {
    const apiUrl = this.baseUrlHoraire + '/horaireEmploye/' + employe;
    return this.http.put<any[]>(apiUrl, updatedHoraire);
  }
  
  ///////////////////////////////////////////////////////////////////////
  getEmployesByService(idService: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/byService/' + idService;
    return this.http.get<any[]>(apiUrl);
  }
}
