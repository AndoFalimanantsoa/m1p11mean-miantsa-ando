import { Component } from '@angular/core';
import { ClientApiServiceService } from '../../Client/Service/service-client/client-api-service.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { RouterModule } from '@angular/router';

@Component({
  selector: 'app-register',
  standalone: true,
  imports: [CommonModule, FormsModule,RouterModule],
  templateUrl: './register.component.html',
  styleUrl: './register.component.css'
})
export class RegisterComponent {
  name: string = "";
  email: string = "";
  password: string = "";
  password1: string = "";
  errorMessage: string="";

  constructor(private myApiService: ClientApiServiceService, private router: Router) {}


  onSubmit() {
    this.register();
  }
  
  register(){
    this.myApiService.register(this.name,this.email,this.password,this.password1).subscribe(
      (response) => {
        console.log('Inscription réussie', response);

        this.router.navigate(['/']);
      },
      (error) => {
        console.error('Connexion refusée', error);
        if (error.error && error.error.message) {
          this.errorMessage = error.error.message; // Utilisez le message d'erreur retourné par votre API
        } else {
          this.errorMessage = 'Une erreur s\'est produite. Veuillez réessayer.'; // Message d'erreur par défaut
        }
      }
    );

  }

}
