import { Component } from '@angular/core';
import { ClientApiServiceService } from '../../Client/Service/service-client/client-api-service.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login',
  standalone: true,
  imports: [CommonModule, FormsModule,RouterModule],
  templateUrl: './login.component.html',
  styleUrl: './login.component.css'
})
export class LoginComponent {
  username: string = "";
  password: string = "";
  errorMessage: string="";

  constructor(private myApiService: ClientApiServiceService, private router : Router) {}

  onSubmit() {
    this.login();
  }
  
  login(){
    this.myApiService.login(this.username,this.password).subscribe(
      (response) => {
        console.log('Connexion réussie', response);
        localStorage.setItem('clientId', response.clientId);
        this.router.navigate(['/client/']);
      },
      (error) => {
        console.error('Connexion refusée', error);
        // Gérez les erreurs en conséquence
        if (error.error && error.error.message) {
          this.errorMessage = error.error.message; // Utilisez le message d'erreur retourné par votre API
        } else {
          this.errorMessage = 'Une erreur s\'est produite lors de la connexion. Veuillez réessayer.'; // Message d'erreur par défaut
        }
      }
    );

  }

}
