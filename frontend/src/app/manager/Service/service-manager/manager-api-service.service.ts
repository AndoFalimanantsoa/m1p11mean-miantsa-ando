import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ManagerApiServiceService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/services';
  private baseUrlEmp = 'https://m1p11mean-miantsa-ando.onrender.com/api/employes';
  private baseUrlMan = 'https://m1p11mean-miantsa-ando.onrender.com/api/managers';

  constructor(private http: HttpClient) { }

  login(email: string, password: string): Observable<any> {
    const apiUrl = this.baseUrlMan + '/login';
    return this.http.post<any>(apiUrl, {email, password});
  }

  getServices(): Observable<any[]> {
    const apiUrl = this.baseUrl + '/';
    return this.http.get<any[]>(apiUrl);
  }

  addService(serviceData: any): Observable<any> {
    const formData = new FormData();

    formData.append('nomservice', serviceData.nomservice);
    formData.append('duree', serviceData.duree);
    formData.append('prix', serviceData.prix);
    formData.append('commission', serviceData.commission);
    formData.append('image', serviceData.image);

    const apiUrl = this.baseUrl + '/';
    return this.http.post<any>(apiUrl, formData);
  }

  updateService(serviceId: string, serviceData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/service/' + serviceId;
    return this.http.put<any>(apiUrl, serviceData);
  }

  deleteService(serviceId: string): Observable<any> {
    const apiUrl = this.baseUrl + '/service/' + serviceId;
    return this.http.delete<any>(apiUrl);
  }

  ////////////////////////////////    Employe  //////////////////////////////////////////
  getEmployes(): Observable<any[]> {
    const apiUrl = this.baseUrlEmp + '/';
    return this.http.get<any[]>(apiUrl);
  }

  addEmploye(name: string, image: File, password: string, services: string[]): Observable<any> {
    const formData = new FormData();

    formData.append('name', name);
    formData.append('image', image);
    formData.append('password', password);
    services.forEach(serviceId => {
      formData.append('services', serviceId);
    });
    console.log(formData);
    const apiUrl = this.baseUrlEmp + '/';
    return this.http.post<any>(apiUrl, formData);
  }

  updateEmploye(employeId: string, employeData: any): Observable<any> {
    const apiUrl = this.baseUrlEmp + '/employe/' + employeId;
    return this.http.put<any>(apiUrl, employeData);
  }

  deleteEmploye(employeId: string): Observable<any> {
    const apiUrl = this.baseUrlEmp + '/employe/' + employeId;
    return this.http.delete<any>(apiUrl);
  }
  //////////////////////////////    End Employe     ///////////////////////////////////////

    ////////////////////    Horaire     //////////////////////////////////
    private baseUrlHoraire = 'https://m1p11mean-miantsa-ando.onrender.com/api/horairesEmployes';
    addHoraire(employe: string): Observable<any[]> {
      const apiUrl = this.baseUrlHoraire + '/';
      return this.http.post<any[]>(apiUrl, {employe:employe, matineeDebut: null, matineeFin: null, apresMidiDebut: null, apresMidiFin: null});
    }
}
