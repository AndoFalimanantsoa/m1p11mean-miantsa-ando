import { TestBed } from '@angular/core/testing';

import { ManagerApiServiceService } from './manager-api-service.service';

describe('ManagerApiServiceService', () => {
  let service: ManagerApiServiceService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ManagerApiServiceService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
