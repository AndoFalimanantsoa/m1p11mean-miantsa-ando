import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OffreService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/offres';

  constructor(private http: HttpClient) { }

  getOffres(): Observable<any[]> {
    const apiUrl = this.baseUrl + '/';
    return this.http.get<any[]>(apiUrl);
  }

  addOffre(offreData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/';
    return this.http.post<any>(apiUrl, offreData);
  }

  updateOffre(offreId: string, offreData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/offre/' + offreId;
    return this.http.put<any>(apiUrl, offreData);
  }

  deleteOffre(offreId: string): Observable<any> {
    const apiUrl = this.baseUrl + '/offre/' + offreId;
    return this.http.delete<any>(apiUrl);
  }
}
