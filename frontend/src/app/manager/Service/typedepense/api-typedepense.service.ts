import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiTypedepenseService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/typedepenses';

  constructor(private http: HttpClient) { }

  getTypeDepenses(): Observable<any[]> {
    const apiUrl = this.baseUrl + '/';
    return this.http.get<any[]>(apiUrl);
  }

  addTypeDepense(typedepenseData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/';
    return this.http.post<any>(apiUrl, typedepenseData);
  }

  updateTypeDepense(typedepenseId: string, typedepenseData: any): Observable<any> {
    const apiUrl = this.baseUrl + '/typedepense/' + typedepenseId;
    return this.http.put<any>(apiUrl, typedepenseData);
  }

  deleteTypeDepense(typedepenseId: string): Observable<any> {
    const apiUrl = this.baseUrl + '/typedepense/' + typedepenseId;
    return this.http.delete<any>(apiUrl);
  }
}
