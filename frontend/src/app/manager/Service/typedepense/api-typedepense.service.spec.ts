import { TestBed } from '@angular/core/testing';

import { ApiTypedepenseService } from './api-typedepense.service';

describe('ApiTypedepenseService', () => {
  let service: ApiTypedepenseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ApiTypedepenseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
