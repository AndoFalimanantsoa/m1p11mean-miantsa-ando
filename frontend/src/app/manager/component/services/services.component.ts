import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ManagerApiServiceService } from '../../Service/service-manager/manager-api-service.service';

@Component({
  selector: 'app-services',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './services.component.html',
  styleUrl: './services.component.css'
})
export class ServicesComponent implements OnInit{
  services: any[] = [];
  newService = { nomservice: '', image:'', duree: '', prix: '', commission:'' };
  selectedService: any = {};


  constructor(private myApiService: ManagerApiServiceService) {}

  ngOnInit() {
    this.loadServices();
  }

  loadServices(){
    this.myApiService.getServices().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.services = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
  }

  handleFileInput(event: any) {
    const files = event.target.files;
    if (files && files.length > 0) {
      this.newService.image = files[0];
    }
  }

  addService() {
    this.myApiService.addService(this.newService).subscribe(
      (response) => {
        console.log('Service ajouté avec succès:', response);
        // Rafraîchir la liste des services après l'ajout
        this.loadServices();
      },
      (error) => {
        console.error('Erreur lors de l\'ajout du service:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

  updateService(serviceId: string): void {
    const updatedServiceData = {
      nomservice: this.selectedService.name,
      duree: this.selectedService.duree,
      prix: this.selectedService.prix,
      commission: this.selectedService.commission,
    };
    this.myApiService.updateService(serviceId, updatedServiceData).subscribe(
      (response) => {
        console.log('Citation mise à jour avec succès:', response);
      },
      (error) => {
        console.error('Erreur lors de la mise à jour du service:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

   // Méthode pour supprimer une citation
  deleteService(serviceId: string): void {
    this.myApiService.deleteService(serviceId).subscribe(
      (response) => {
        console.log('Service supprimé avec succès:', response);
        // Rafraîchir la liste des citations après la suppression
        this.loadServices();
      },
      (error) => {
        console.error('Erreur lors de la suppression de la citation:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }
}
