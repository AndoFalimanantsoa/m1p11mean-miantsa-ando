import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ManagerApiServiceService } from '../../Service/service-manager/manager-api-service.service';

@Component({
  selector: 'app-employes',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './employes.component.html',
  styleUrl: './employes.component.css'
})
export class EmployesComponent implements OnInit{
  employes: any[] = [];
  services: any[] = [];
  name: string = '';
  image: File | null = null;
  selectedEmploye: any = {};
  selectedServices: string[] = [];


  constructor(private myApiService: ManagerApiServiceService) {}

  ngOnInit() {
    this.loadEmployes();
    this.loadServices();
  }

  loadEmployes(){
    this.myApiService.getEmployes().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.employes = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
  }

  loadServices() {
    // Méthode pour charger la liste des services disponibles depuis le service
    this.myApiService.getServices().subscribe(
      (data: any[]) => {
        console.log('Données des services récupérées avec succès :', data);
        this.services = data;
      },
      (error) => {
        console.error('Erreur côté client lors du chargement des services:', error);
      }
    );
  }

  onFileSelected(event: any) {
    if (event.target.files[0]) {
      this.image = event.target.files[0];
      if (this.image) {
        console.log('blablabla ' + this.image.name);
      }
      
    }
  }
  onServiceSelected(event: any) {
    const serviceId = event.target.value;
    if (event.target.checked) {
      // Si le service est coché, l'ajouter à la liste des services sélectionnés
      this.selectedServices.push(serviceId);
    } else {
      // Si le service est décoché, le retirer de la liste des services sélectionnés
      const index = this.selectedServices.indexOf(serviceId);
      if (index !== -1) {
        this.selectedServices.splice(index, 1);
      }
    }
  }

  addEmploye() {
    if (!this.image) {
      console.error('Aucun fichier selectionné.');
      return;
    }
    this.myApiService.addEmploye(this.name, this.image, '',this.selectedServices).subscribe(
      (response) => {
        console.log('Employé ajouté avec succès:', response);
        const employeId= response.employeId;
        this.generateHoraire(employeId);
        this.loadEmployes();
      },
      (error) => {
        console.error('Erreur lors de l\'ajout de l\'employé:', error);
      }
    );
  }

  updateEmploye(employeId: string): void {
    const updatedEmployeData = {
      name: this.selectedEmploye.name,
    };
    this.myApiService.updateEmploye(employeId, updatedEmployeData).subscribe(
      (response) => {
        console.log('Employé mise à jour avec succès:', response);
        this.loadEmployes();
      },
      (error) => {
        console.error('Erreur lors de la mise à jour de l\'employé:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

  generateHoraire(employeId: string){
    this.myApiService.addHoraire(employeId).subscribe(
      (response) => {
        console.log('Horaire initié avec succès', response);
      },
      (error) => {
        console.error('Erreur lors de l\' initiation des horaires: ', error);
      }
    );
  }

   // Méthode pour supprimer une citation
  deleteEmploye(employeId: string): void {
    this.myApiService.deleteEmploye(employeId).subscribe(
      (response) => {
        console.log('Employe supprimé avec succès:', response);
        // Rafraîchir la liste des citations après la suppression
        this.loadEmployes();
      },
      (error) => {
        console.error('Erreur lors de la suppression de l\'employé:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }
}
