import { Component, OnInit } from '@angular/core';
import { OffreService } from '../../Service/offre/offre.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ManagerApiServiceService } from '../../Service/service-manager/manager-api-service.service';

@Component({
  selector: 'app-offres',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './offres.component.html',
  styleUrl: './offres.component.css'
})
export class OffresComponent implements OnInit {
  offres: any[] = [];
  services: any[] = [];
  newOffre = { idservice: '', datedebut: '', datefin: '', remise: '' };
  selectedTypeDepense: any = {};
  
  selectedServiceId: string = '';

  constructor(private myApiService: OffreService, private serviceService: ManagerApiServiceService) {}

  ngOnInit(): void {
    this.loadOffres();
    this.loadServices();
  }
  loadServices() {
    this.serviceService.getServices().subscribe(
      (services) => {
        if (Array.isArray(services)) {
          this.services = services;
          console.log('Services chargés avec succès:', this.services);
        } else {
          console.error('La réponse du service n\'est pas un tableau.');
        }
      },
      (error) => {
        console.error('Erreur lors du chargement des services', error);
      }
    );
  }
  
  loadOffres(){
    this.myApiService.getOffres().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.offres = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
  }

  addOffre() {
    this.newOffre.idservice = this.selectedServiceId;
    this.myApiService.addOffre(this.newOffre).subscribe(
      (response) => {
        console.log('Offre ajouté avec succès:', response);
        // Rafraîchir la liste des offre après l'ajout
        this.loadOffres();
      },
      (error) => {
        console.error('Erreur lors de l\'ajout de l\'offre:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

  updateOffre(offreId: string): void {
    const updatedOffreData = {
      nomtypedepense: this.selectedTypeDepense.nomtypedepense,
    };
    this.myApiService.updateOffre(offreId, updatedOffreData).subscribe(
      (response) => {
        console.log('Offre mise à jour avec succès:', response);
      },
      (error) => {
        console.error('Erreur lors de la mise à jour de l\'offre:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

   // Méthode pour supprimer une offre
   deleteOffre(offreId: string): void {
    this.myApiService.deleteOffre(offreId).subscribe(
      (response) => {
        console.log('Offre supprimée avec succès:', response);
        // Rafraîchir la liste des types de dépense après la suppression
        this.loadOffres();
      },
      (error) => {
        console.error('Erreur lors de la suppression de l\'offre:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }
}
