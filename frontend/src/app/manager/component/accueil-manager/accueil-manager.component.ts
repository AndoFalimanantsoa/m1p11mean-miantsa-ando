import { Component } from '@angular/core';
import { RouterLink, RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-accueil-manager',
  standalone: true,
  imports: [RouterOutlet, RouterLink],
  templateUrl: './accueil-manager.component.html',
  styleUrl: './accueil-manager.component.css'
})
export class AccueilManagerComponent {

}
