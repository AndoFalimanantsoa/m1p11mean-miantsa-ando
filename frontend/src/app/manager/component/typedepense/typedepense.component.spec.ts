import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypedepenseComponent } from './typedepense.component';

describe('TypedepenseComponent', () => {
  let component: TypedepenseComponent;
  let fixture: ComponentFixture<TypedepenseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [TypedepenseComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(TypedepenseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
