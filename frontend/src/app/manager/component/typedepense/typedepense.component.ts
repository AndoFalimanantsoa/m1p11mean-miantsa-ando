import { Component, OnInit } from '@angular/core';
import { ApiTypedepenseService } from '../../Service/typedepense/api-typedepense.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-typedepense',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './typedepense.component.html',
  styleUrl: './typedepense.component.css'
})
export class TypedepenseComponent implements OnInit{
  typedepenses: any[] = [];
  newTypeDepense = { nomtypedepense: '' };
  selectedTypeDepense: any = {};

  constructor(private myApiService: ApiTypedepenseService) {}

  ngOnInit() {
    this.loadTypeDepenses();
  }

  loadTypeDepenses(){
    this.myApiService.getTypeDepenses().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.typedepenses = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
  }

  addTypeDepense() {
    this.myApiService.addTypeDepense(this.newTypeDepense).subscribe(
      (response) => {
        console.log('TypeDepense ajouté avec succès:', response);
        // Rafraîchir la liste des services après l'ajout
        this.loadTypeDepenses();
      },
      (error) => {
        console.error('Erreur lors de l\'ajout du service:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

  updateTypeDepense(typeDepenseId: string): void {
    const updatedTypeDepenseData = {
      nomtypedepense: this.selectedTypeDepense.nomtypedepense,
    };
    this.myApiService.updateTypeDepense(typeDepenseId, updatedTypeDepenseData).subscribe(
      (response) => {
        console.log('Type dépense mis à jour avec succès:', response);
      },
      (error) => {
        console.error('Erreur lors de la mise à jour du type de dépense:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }

   // Méthode pour supprimer un type de depense
   deleteTypeDepense(typeDepenseId: string): void {
    this.myApiService.deleteTypeDepense(typeDepenseId).subscribe(
      (response) => {
        console.log('Type de dépense supprimé avec succès:', response);
        // Rafraîchir la liste des types de dépense après la suppression
        this.loadTypeDepenses();
      },
      (error) => {
        console.error('Erreur lors de la suppression de la citation:', error);
        // Gérez les erreurs en conséquence
      }
    );
  }
}
