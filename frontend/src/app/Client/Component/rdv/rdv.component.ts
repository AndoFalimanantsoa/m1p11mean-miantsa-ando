import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CalendarModule } from 'angular-calendar';
import { ManagerApiServiceService } from '../../../manager/Service/service-manager/manager-api-service.service';
import { EmployeApiServiceService } from '../../../Employe/Service/service-employe/employe-api-service.service.service';

@Component({
  selector: 'app-rdv',
  standalone: true,
  imports: [CommonModule, FormsModule, CalendarModule],
  templateUrl: './rdv.component.html',
  styleUrl: './rdv.component.css'
})
export class RdvComponent implements OnInit {
  etapes: any[] = [
    { nom: 'Choisissez une option', contenu: 'Contenu de l\'étape 1...' },
    { nom: 'Remplissez le formulaire', contenu: 'Contenu de l\'étape 2...' },
    { nom: 'Confirmez votre commande', contenu: 'Contenu de l\'étape 3...' },
    { nom: 'Paiement', contenu: 'Contenu de l\'étape 4...' },
  ];

  selectedEtape: any | null = null;
  services: any[] = [];
  servicesSelectionnes: any[] = [];
  employesCorrespondants: { [key: string]: any[] } = {};

  heures = Array.from({ length: 18 }, (_, i) => {
    const hour = Math.floor(i / 2) + 8; // Heure
    const minute = i % 2 === 0 ? '00' : '30'; // Minute
    return `${hour}:${minute}`;
  });

  viewDate: Date = new Date();
  selectedDate: Date | null = null
  currentYear: number = new Date().getFullYear(); // Initialize currentYear
  currentMonth: number = new Date().getMonth() + 1; // Initialize currentMonth
  weeks: number[][] = [];
  days: string[] = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  months: string[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

  constructor(private serviceService: ManagerApiServiceService, private employeService: EmployeApiServiceService) {
    this.generateCalendar();
  }

  ngOnInit(): void {
    // Initialiser selectedEtape avec la première étape
    this.selectedEtape = this.etapes[0];
    this.generateCalendar();
    this.serviceService.getServices().subscribe(
      (services) => {
        this.services = services;
      },
      (error) => {
        console.error('Erreur lors du chargement des services', error);
      }
    );
  }

  getMonthYearTitle(): string {
    const months = [
      'Janvier', 'Février', 'Mars', 'Avril',
      'Mai', 'Juin', 'Juillet', 'Août',
      'Septembre', 'Octobre', 'Novembre', 'Decembre'
    ];

    return `${months[this.currentMonth - 1]} ${this.currentYear}`;
  }

  validerEtape(etape: any): void {
    console.log(`Étape ${etape.nom} validée.`);
    this.servicesSelectionnes = this.services.filter(service => service.checked);
    for (const selectedService of this.servicesSelectionnes) {
      console.log("idService: "+ selectedService._id+" NomService: "+selectedService.nomservice);
      this.employeService.getEmployesByService(selectedService._id)
        .subscribe(employes => {
          this.employesCorrespondants[selectedService._id] = employes;
        });
    }
    this.passerALaProchaineEtape();
  }
  
  passerALaProchaineEtape(): void {
    if (this.selectedEtape) {
      const indexCourant = this.etapes.findIndex(etape => etape.nom === this.selectedEtape.nom);
      if (indexCourant < this.etapes.length - 1) {
        // Passer à l'étape suivante
        this.selectedEtape = this.etapes[indexCourant + 1];
      }
    }
  }
  retournerALaEtapePrecedente(): void {
    if (this.selectedEtape) {
      const indexCourant = this.etapes.indexOf(this.selectedEtape);
      if (indexCourant > 0) {
        // Revenir à l'étape précédente
        this.selectedEtape = this.etapes[indexCourant - 1];
      }
    }
  }



  generateCalendar(): void {
    const firstDay = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth(), 1);
    const lastDay = new Date(this.viewDate.getFullYear(), this.viewDate.getMonth() + 1, 0);
    const daysInMonth = lastDay.getDate();
    const firstDayIndex = firstDay.getDay();
    const lastDayIndex = lastDay.getDay();

    let day = 1;
    this.weeks = [];

    for (let i = 0; i < 6; i++) {
      const week: number[] = [];

      for (let j = 0; j < 7; j++) {
        if (i === 0 && j < firstDayIndex) {
          week.push(0); // Placeholder for previous month's days
        } else if (day > daysInMonth) {
          week.push(0); // Placeholder for next month's days
        } else {
          week.push(day);
          day++;
        }
      }

      this.weeks.push(week);
    }
  }
  changeMonth(offset: number): void {
    this.currentMonth += offset;
    if (this.currentMonth === 0) {
      this.currentMonth = 12;
      this.currentYear -= 1;
    }
    if (this.currentMonth === 13) {
      this.currentMonth = 1;
      this.currentYear += 1;
    }
    this.viewDate.setMonth(this.viewDate.getMonth() + offset);
    this.generateCalendar();
  }

  selectDate(event: any): void {
    if (this.selectedDate) {
      this.selectedDate = null;
    }

    // Sélectionnez la nouvelle date
    this.selectedDate = event;
  }
  
}
