import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ClientApiServiceService } from '../../Service/service-client/client-api-service.service';


@Component({
  selector: 'app-employes-list',
  standalone: true,
  imports: [CommonModule,FormsModule],
  templateUrl: './employes.component.html',
  styleUrls: ['./employes.component.css']
})
export class EmployesListComponent implements OnInit {
  services: any[] = [];
  selectedService: string='';
  employes: any[] = [];
  clientId: string='';

  constructor(private myApiService: ClientApiServiceService) { }

  ngOnInit(): void {
    this.loadServices();
  }

  loadServices() {
    this.myApiService.getServices().subscribe(
      (data: any[]) => {
        this.services = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
      }
    );
  }

  onServiceSelected(service: string) {
    this.selectedService=service;
    this.loadEmployesByService(this.selectedService);
  }

  loadEmployesByService(serviceId: string) {
    this.myApiService.getEmployesByService(serviceId).subscribe(
      (data: any[]) => {
        this.employes = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
      }
    );
  }

  addPreference(employeId: string, note: number, serviceId: string) {
      // Récupérer l'ID du client depuis le localStorage
    const clientId = localStorage.getItem('clientId');
    console.log(clientId);
    if (!clientId) {
      console.error('ID client non trouvé dans le localStorage.');
      return;
    }

    // Envoyer la préférence (notation) au backend
    this.myApiService.addPreference(note,clientId, serviceId, employeId).subscribe(
      (response) => {
        console.log('Préférence ajoutée avec succès :', response);
        // Rafraîchir la liste des employés après avoir ajouté la préférence
        this.loadEmployesByService(this.selectedService);
      },
      (error) => {
        console.error('Erreur lors de l\'ajout de la préférence :', error);
        // Gérer les erreurs en conséquence
      }
    );
  }
}
