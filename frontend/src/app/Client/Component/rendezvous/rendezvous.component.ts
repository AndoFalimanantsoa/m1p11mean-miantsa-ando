import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-rendezvous',
  standalone: true,
  imports: [CommonModule, FormsModule],
  templateUrl: './rendezvous.component.html',
  styleUrl: './rendezvous.component.css'
})
export class RendezvousComponent implements OnInit{
  calendar: number[][] = [];
  currentYear: number = new Date().getFullYear(); // Initialize currentYear
  currentMonth: number = new Date().getMonth() + 1; // Initialize currentMonth
  selectedDate: string | null = null; // Initialize to null

    // Nouveaux champs pour le formulaire
    taskName: string = '';
    startDate: string = '';
    endDate: string = '';


    heures = Array.from({ length: 24 }, (_, i) => i < 12 ? `${i + 1}am` : `${i - 11}pm`);
    currentDate: Date = new Date();
    jours = ['Dimanche', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi'];
    currentDiv: number = 1;



// Liste des tâches
tasks: { date: string, names: string[] }[] = [
  { date: '2024-02-15', names: ['Test', 'AnotherTask'] }
];


  constructor() { }

  ngOnInit(): void {
    this.updateCalendar();
  }

  
  showDiv(divNumber: number): void {
    this.currentDiv = divNumber;
    // Ajoutez d'autres mises à jour en conséquence (menu, etc.) si nécessaire
  }

  updateCalendar() {
    this.calendar = []; // Reset the calendar
    const firstDay = new Date(this.currentYear, this.currentMonth - 1, 1).getDay();
    const lastDay = new Date(this.currentYear, this.currentMonth, 0).getDate();
    let dayCount = 1;

    for (let week = 0; week < 6; week++) {
      const row: number[] = [];

      for (let day = 0; day < 7; day++) {
        if ((week === 0 && day >= firstDay) || (week > 0 && dayCount <= lastDay)) {
          row.push(dayCount);
          dayCount++;
        } else {
          row.push(0); // Placeholder for empty cells
        }
      }

      this.calendar.push(row);
    }
  }

  goToPreviousMonth() {
    this.currentMonth -= 1;
    if (this.currentMonth === 0) {
      this.currentMonth = 12;
      this.currentYear -= 1;
    }
    this.updateCalendar();
  }

  goToNextMonth() {
    this.currentMonth += 1;
    if (this.currentMonth === 13) {
      this.currentMonth = 1;
      this.currentYear += 1;
    }
    this.updateCalendar();
  }

  getMonthYearTitle(): string {
    const months = [
      'Janvier', 'Février', 'Mars', 'Avril',
      'Mai', 'Juin', 'Juillet', 'Août',
      'Septembre', 'Octobre', 'Novembre', 'Decembre'
    ];

    return `${months[this.currentMonth - 1]} ${this.currentYear}`;
  }

  openPopup(day: number): void {
    if (day !== 0) {
      this.selectedDate = `${this.currentYear}-${this.currentMonth}-${day}`;
    } else {
      this.selectedDate = null;
    }
  }
  closePopup(): void {
    this.selectedDate = null;
  }

  addTask(): void {
    // Rechercher si une tâche existe déjà pour la date sélectionnée
    const existingTask = this.tasks.find(task => task.date === this.selectedDate);

    // Si une tâche existe déjà, ajouter le nouveau nom à la liste
    if (existingTask) {
      existingTask.names.push(this.taskName);
    } else {
      // Sinon, créer une nouvelle entrée pour la date avec le nom de la tâche
      this.tasks.push({
        date: this.selectedDate!,
        names: [this.taskName]
      });
    }

    // Réinitialiser le formulaire après l'ajout
    this.resetForm();
    this.updateCalendar(); // Mise à jour du calendrier après l'ajout de la tâche
  }


  resetForm(): void {
    this.taskName = '';
    this.startDate = '';
    this.endDate = '';
  }

   // Vérifier si une tâche existe pour une journée donnée
   hasTask(day: number): boolean {
    const currentDate = `${this.currentYear}-${this.currentMonth}-${day}`;
    return this.tasks.some(task => task.date === currentDate);
  }

  // Récupérer la liste des noms de tâches pour une journée donnée
  getTaskNames(day: number): string[] | undefined {
    const currentDate = `${this.currentYear}-${this.currentMonth}-${day}`;
    const task = this.tasks.find(task => task.date === currentDate);
    return task?.names;
  }

  getDayForHour(): string {
    // Utilisez la date actuelle
    const formattedDate = this.currentDate.toLocaleDateString('fr-FR', { weekday: 'long', day: 'numeric', month: 'long', year: 'numeric' });
    return formattedDate;
  }

  incrementDate(): void {
    // Ajoutez un jour à la date actuelle
    this.currentDate.setDate(this.currentDate.getDate() + 1);
  }

  decrementDate(): void {
    // Soustrayez un jour à la date actuelle
    this.currentDate.setDate(this.currentDate.getDate() - 1);
  }

  getDayAndHour(day: string): string {
    const dayIndex = this.jours.indexOf(day);
    const newDate = new Date(this.currentDate);
    newDate.setDate(this.currentDate.getDate() - (this.currentDate.getDay() - (dayIndex === 0 ? 0 : dayIndex)));
  
    return newDate.toLocaleDateString('fr-FR', { weekday: 'short', day: 'numeric', month: 'numeric' });
  }
  
  getTableTitle(): string {
    const startOfWeek = new Date(this.currentDate);
    startOfWeek.setDate(this.currentDate.getDate() - (this.currentDate.getDay()));
    const endOfWeek = new Date(startOfWeek);
    endOfWeek.setDate(startOfWeek.getDate() + 6);

    return `${startOfWeek.toLocaleDateString('fr-FR', { month: 'short', day: 'numeric' })} – ${endOfWeek.toLocaleDateString('en-US', { month: 'short', day: 'numeric', year: 'numeric' })}`;
  }

  nextWeek(): void {
    this.currentDate.setDate(this.currentDate.getDate() + 7);
  }

  previousWeek(): void {
    this.currentDate.setDate(this.currentDate.getDate() - 7);
  }

}
