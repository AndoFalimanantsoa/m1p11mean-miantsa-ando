import { Component, OnInit } from '@angular/core';
import { OffreService } from '../../../manager/Service/offre/offre.service';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-offreclient',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './offreclient.component.html',
  styleUrl: './offreclient.component.css'
})
export class OffreclientComponent implements OnInit{
  offres: any[] = [];

  constructor(private myApiService: OffreService) {}

  ngOnInit(): void {
    this.loadOffres();
  }
  loadOffres(){
    this.myApiService.getOffres().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.offres = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
}
}
