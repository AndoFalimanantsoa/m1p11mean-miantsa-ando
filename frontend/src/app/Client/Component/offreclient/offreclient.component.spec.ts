import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OffreclientComponent } from './offreclient.component';

describe('OffreclientComponent', () => {
  let component: OffreclientComponent;
  let fixture: ComponentFixture<OffreclientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [OffreclientComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(OffreclientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
