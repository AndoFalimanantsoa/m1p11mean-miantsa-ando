import { CommonModule } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ManagerApiServiceService } from '../../../manager/Service/service-manager/manager-api-service.service';

@Component({
  selector: 'app-serviceclient',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './serviceclient.component.html',
  styleUrl: './serviceclient.component.css'
})
export class ServiceclientComponent implements OnInit{
  services: any[] = [];

  constructor(private myApiService: ManagerApiServiceService) {}

  ngOnInit() {
    this.loadServices();
  }

  loadServices(){
    this.myApiService.getServices().subscribe(
      (data: any[]) => {
        console.log('Données récupérées avec succès :', data);
        this.services = data;
      },
      (error) => {
        console.error('Erreur côté client :', error);
        console.error('Réponse serveur :', error.error);
        // Affichez l'erreur pour en savoir plus
      }
    );
  }
}
