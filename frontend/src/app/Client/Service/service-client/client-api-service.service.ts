import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ClientApiServiceService {
  private baseUrl = 'https://m1p11mean-miantsa-ando.onrender.com/api/clients';

  constructor(private http: HttpClient) {}

  login(email: string, password: string): Observable<any> {
    const apiUrl = this.baseUrl + '/login';
    return this.http.post<any>(apiUrl, {email, password});
  }

  register(name: string,email: string, password: string, password1: string): Observable<any> {
    const apiUrl = this.baseUrl + '/register';
    return this.http.post<any>(apiUrl, {name, email,password, password1});
  }


  getClients(): Observable<any[]> {
    const apiUrl = this.baseUrl + '/';
    return this.http.get<any[]>(apiUrl);
  }

  getClientById(idClient: string): Observable<any[]> {
    const apiUrl = this.baseUrl + '/client/' + idClient;
    return this.http.get<any[]>(apiUrl);
  }
  ///////////////////////////   Services    ///////////////////////////////////////////////
  private baseUrlServ = 'https://m1p11mean-miantsa-ando.onrender.com/api/services';
  getServices(): Observable<any[]> {
    const apiUrl = this.baseUrlServ + '/';
    return this.http.get<any[]>(apiUrl);
  }
  ///////////////////////////  End Service ///////////////////////////////////////////////


  ////////////////////////// Employe - Service  /////////////////////////////////////////
  private baseUrlEmp = 'https://m1p11mean-miantsa-ando.onrender.com/api/employes';
  getEmployesByService(serviceId: string): Observable<any[]> {
    const apiUrl = this.baseUrlEmp + '/byService/'+ serviceId;
    return this.http.get<any[]>(apiUrl);
  }
  ///////////////////////////////////// END    /////////////////////////////////////


  /////////////////////////   Preference    ///////////////////////////////////////
  private baseUrlPref = 'https://m1p11mean-miantsa-ando.onrender.com/api/preferences';

  addPreference(note:number,clientId: string, serviceId: string, employeId: string): Observable<any> {
    const apiUrl = this.baseUrlPref + '/';
    return this.http.post<any>(apiUrl, { note, clientId, serviceId, employeId});
  }


  //////////////////////// End Preference   ///////////////////////////////////////
}
