import { Routes } from '@angular/router';
import { AccueilManagerComponent } from './manager/component/accueil-manager/accueil-manager.component';
import { AccueilEmployeComponent } from './Employe/Component/accueil-employe/accueil-employe.component';
import { ServicesComponent } from './manager/component/services/services.component';
import { EmployesComponent } from './manager/component/employes/employes.component';
import { ClientComponent } from './Client/Component/client/client.component';
import { ProfilComponent } from './Employe/Component/profil/profil.component';
import { HoraireComponent } from './Employe/Component/horaire/horaire.component';
import { AccueilComponent } from './Client/Component/accueil/accueil.component';
import { ServiceclientComponent } from './Client/Component/serviceclient/serviceclient.component';
import { EmployesListComponent } from './Client/Component/employes/employes.component';
import { RendezvousComponent } from './Client/Component/rendezvous/rendezvous.component';
import { TypedepenseComponent } from './manager/component/typedepense/typedepense.component';
import { LoginComponent } from './Connexion/login/login.component';
import { LoginEmployeComponent } from './ConnexionEmploye/login-employe/login-employe.component';
import { LoginManagerComponent } from './ConnexionManager/login-manager/login-manager.component';
import { RegisterComponent } from './Connexion/register/register.component';
import { OffresComponent } from './manager/component/offres/offres.component';
import { OffreclientComponent } from './Client/Component/offreclient/offreclient.component';
import { RendezvousEmployeComponent } from './Employe/Component/rendezvous-employe/rendezvous-employe.component';
import { RdvComponent } from './Client/Component/rdv/rdv.component';
import { ReservationComponent } from './manager/reservation/reservation.component';

export const routes: Routes = [
    {
        path:'',
        component: LoginComponent
    },
    {
        path:'loginEmploye',
        component: LoginEmployeComponent
    },
    {
        path:'loginManager',
        component: LoginManagerComponent
    },
    {
        path:'register',
        component: RegisterComponent
    },
    {
        path: 'client', component: ClientComponent,
        children: [
            { path: '', component: AccueilComponent, pathMatch: 'full' },
            { path: 'accueil', component: AccueilComponent, pathMatch: 'full' },
            { path: 'serviceclient', component: ServiceclientComponent, pathMatch: 'full' },
            { path: 'offreclient', component: OffreclientComponent, pathMatch: 'full' },
            { path: 'rendezvous', component: RendezvousComponent, pathMatch: 'full' },
            { path: 'rdv', component: RdvComponent, pathMatch: 'full' },
            { path: 'employes', component: EmployesListComponent, pathMatch: 'full' }
          ],
    },
    {
        path: 'manager',
        component: AccueilManagerComponent,
        children: [
            { path: 'services', component: ServicesComponent, pathMatch: 'full' },
            { path: 'offres', component: OffresComponent, pathMatch: 'full' },
            { path: 'employes', component: EmployesComponent, pathMatch: 'full' },
            { path: 'typedepense', component: TypedepenseComponent, pathMatch: 'full' },
            { path: 'reservation', component: ReservationComponent, pathMatch: 'full' },
          ],
    },
    {
        path: 'employe',
        component: AccueilEmployeComponent,
        children: [
            { path: '', component: ProfilComponent, pathMatch: 'full' },
            { path: 'horaire', component: HoraireComponent, pathMatch: 'full' },
            { path: 'rendezvous', component: RendezvousEmployeComponent, pathMatch: 'full' },
        ],
    },
];
