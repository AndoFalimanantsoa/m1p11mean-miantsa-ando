import { Component } from '@angular/core';
import { ManagerApiServiceService } from '../../manager/Service/service-manager/manager-api-service.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-manager',
  standalone: true,
  imports: [CommonModule, FormsModule,RouterModule],
  templateUrl: './login-manager.component.html',
  styleUrl: './login-manager.component.css'
})
export class LoginManagerComponent {
  email: string = "";
  password: string = "";
  errorMessage: string="";

  constructor(private myApiService: ManagerApiServiceService, private router : Router) {}

  onSubmit() {
    this.login();
  }
  
  login(){
    this.myApiService.login(this.email,this.password).subscribe(
      (response) => {
        console.log('Connexion réussie', response);
        localStorage.setItem('managerId', response.managerId);
        this.router.navigate(['/manager']);
      },
      (error) => {
        console.error('Connexion refusée', error);
        // Gérez les erreurs en conséquence
        if (error.error && error.error.message) {
          this.errorMessage = error.error.message; // Utilisez le message d'erreur retourné par votre API
        } else {
          this.errorMessage = 'Une erreur s\'est produite lors de la connexion. Veuillez réessayer.'; // Message d'erreur par défaut
        }
      }
    );

  }

}
