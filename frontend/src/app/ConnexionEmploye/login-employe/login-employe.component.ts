import { Component } from '@angular/core';
import { EmployeApiServiceService } from '../../Employe/Service/service-employe/employe-api-service.service.service';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { Router } from '@angular/router';


@Component({
  selector: 'app-login-employe',
  standalone: true,
  imports: [CommonModule, FormsModule,RouterModule],
  templateUrl: './login-employe.component.html',
  styleUrl: './login-employe.component.css'
})
export class LoginEmployeComponent {
  name: string = "";
  password: string = "";
  errorMessage: string="";

  constructor(private myApiService: EmployeApiServiceService, private router : Router) {}

  onSubmit() {
    this.login();
  }
  
  login(){
    this.myApiService.login(this.name,this.password).subscribe(
      (response) => {
        console.log('Connexion réussie', response);
        localStorage.setItem('employeId', response.employeId);
        this.router.navigate(['/employe/']);
      },
      (error) => {
        console.error('Connexion refusée', error);
        // Gérez les erreurs en conséquence
        if (error.error && error.error.message) {
          this.errorMessage = error.error.message; // Utilisez le message d'erreur retourné par votre API
        } else {
          this.errorMessage = 'Une erreur s\'est produite lors de la connexion. Veuillez réessayer.'; // Message d'erreur par défaut
        }
      }
    );

  }

}
